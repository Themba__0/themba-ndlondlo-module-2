import 'dart:io';

void main(List<String> args) {
  print('Enter app name: \n>>>>>>>');
  String app = stdin.readLineSync().toString();
  print('Enter developer name: \n>>>>>>>');
  String devName = stdin.readLineSync().toString();
  print('Enter the sector it falls under: \n>>>>>>>');
  String sector = stdin.readLineSync().toString();
  print('Enter the year it won: \n>>>>>>>');
  String yr = stdin.readLineSync().toString();
  var year = int.parse(yr);

  winningApp(app, devName, sector, year);
}

class winningApp {
  var appName = '';
  var year = 0;
  var sector = '';
  var developer = '';

  winningApp(String appName, String developer, String sector, int year) {
    print(
        'The winning app is:\n${appName.toUpperCase()}\nDeveloper:${developer}:\nYear ${year}\nSector: ${sector}');
  }
}
